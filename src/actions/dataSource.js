import axios from 'axios';
import { apiUrls, actionTypes } from '../constants';

const toggleLoader = () => {
    return {
        type: actionTypes.DATA_SOURCE_TOGGLE_LOADER
    }
}

const setDataSources = (dataSets) => {
    return {
        type: actionTypes.DATA_SOURCE_SET,
        payload: dataSets
    }
}

export const getDataSources = (id) => {
    return async (dispatch) => {
        try {
            const response = await axios
                .get(`${apiUrls.API_URL}${apiUrls.FETCH_ALL_JOBS_API}${id}`);
            dispatch(setDataSources(response.data));
            dispatch(toggleLoader());
        }
        catch (error) {
            dispatch(toggleLoader());
        }
    };
};

export const postDataSources = (id) => {
    return async (dispatch) => {
        dispatch(toggleLoader());
        try {
            const response = await axios.get('https://petstore.swagger.io/v2/pet/findByStatus?status=pending')
            // const response = await axios
            //     .get(`${apiUrls.API_URL}${apiUrls.FETCH_ALL_JOBS_API}${id}`);
            // dispatch(setDataSources(response.data));
            dispatch(toggleLoader());
        }
        catch (error) {
            dispatch(toggleLoader());
        }
    };
};

// export const createJob = (job) => {
//     // function that dispatches an action at a later time
//     return (dispatch) => {
//         // Returns a promise
//         return axios
//             .post(`${apiUrls.API_URL}${apiUrls.CREATE_JOB_API}`, job)
//             .then(response => {
//                 console.log(response);
//                 // Dispatch another action to consume data
//                 if (response.status === 201) {
//                     window.location.reload();
//                     // dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'SUCCESS', message: response.data.message, jobDetails: response.data.data });
//                 } else {
//                     dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'ERROR', message: response.data.message, jobDetails: null });
//                 }
//             })
//             .catch(error => {
//                 dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'ERROR', message: error.toString(), jobDetails: null });
//             });
//     };
// };

// export const updateJob = (job) => {
//     // function that dispatches an action at a later time
//     return (dispatch) => {
//         // Returns a promise
//         return axios
//             .patch(`${apiUrls.API_URL}${apiUrls.UPDATE_JOB_API}`, job)
//             .then(response => {
//                 console.log(response);
//                 // Dispatch another action to consume data
//                 if (response.status === 201) {
//                     window.location.reload();
//                     // dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'SUCCESS', message: response.data.message, jobDetails: response.data.data });
//                 } else {
//                     dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'ERROR', message: response.data.message, jobDetails: null });
//                 }
//             })
//             .catch(error => {
//                 dispatch({ type: actionTypes.GET_ALL_JOBS, status: 'ERROR', message: error.toString(), jobDetails: null });
//             });
//     };
// };

// export const deleteJob = (id) => {
//     console.log(apiUrls);
//     // function that dispatches an action at a later time
//     return (dispatch) => {
//         // Returns a promise
//         return axios
//             .delete(`${apiUrls.API_URL}${apiUrls.DELETE_JOB_API}${id}`)
//             .then(response => {
//                 // Dispatch another action to consume data
//                 if (response.status === 200) {
//                     window.location.reload();
//                     // dispatch({ type: actionTypes.GET_JOB_LISTING_BY_ID, status: 'SUCCESS', message: response.data.message, jobDetails: response.data.data });
//                 } else {
//                     dispatch({ type: actionTypes.GET_JOB_LISTING_BY_ID, status: 'ERROR', message: response.data.message, jobDetails: null });
//                 }
//             })
//             .catch(error => {
//                 dispatch({ type: actionTypes.GET_JOB_LISTING_BY_ID, status: 'ERROR', message: error.toString(), jobDetails: null });
//             });
//     };
// };