import { actionTypes } from '../constants';

const initialState = {
    dataSources : [],
    dataSourcesLoading: false
}
export default (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.DATA_SOURCE_TOGGLE_LOADER:
            return {...state, dataSourcesLoading: !state.dataSourcesLoading};
        default:
            return state
    }
};