import { combineReducers } from 'redux';
import dataSourceReducer from './dataSource';

const rootReducer = combineReducers({ dataSourceReducer });

export default rootReducer