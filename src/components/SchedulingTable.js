import React from 'react';
import PropTypes from 'utils/propTypes';
import { Table, Button } from 'reactstrap';

const RulesTable = ({ headers, schedulingData, ...restProps }) => {
    return (
        <Table responsive hover {...restProps}>
            <thead>
                <tr className="text-capitalize align-middle text-center">
                    {headers.map((item, index) => <th key={index}>{item}</th>)}
                </tr>
            </thead>
            <tbody>
                {schedulingData.map(({ id, jobName, type, status, button }) => (
                    <tr key={id}>
                        <td className="align-middle text-center">{jobName}</td>
                        <td className="align-middle text-center">{type}</td>
                        <td className="align-middle text-center">{status}</td>
                        <td className="align-middle text-center">
                        <Button color='primary' className="btn-sm">{button}</Button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
};

RulesTable.propTypes = {
    headers: PropTypes.node,
    schedulingData: PropTypes.arrayOf(
        PropTypes.shape({
            avatar: PropTypes.string,
            name: PropTypes.string,
            date: PropTypes.date,
        })
    ),
};

RulesTable.defaultProps = {
    headers: [],
    schedulingData: [],
};

export default RulesTable;
