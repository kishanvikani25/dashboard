import React from 'react';
import PropTypes from 'utils/propTypes';
import { Table, Progress } from 'reactstrap';

const StatusTable = ({ headers, statusData, ...restProps }) => {
    return (
        <Table responsive hover {...restProps}>
            <thead>
                <tr className="text-capitalize align-middle text-center">
                    {headers.map((item, index) => <th key={index}>{item}</th>)}
                </tr>
            </thead>
            <tbody>
                {statusData.map(({ id, migration, color, percentage, status }) => (
                    <tr key={id}>
                        <td className="align-middle text-center">{migration}</td>
                        <td className="align-middle text-center">
                            <Progress
                                key={id}
                                animated={status === 'In Progress'}
                                color={color}
                                value={percentage}
                            > {status} - {percentage}%</Progress>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
};

StatusTable.propTypes = {
    headers: PropTypes.node,
    statusData: PropTypes.arrayOf(
        PropTypes.shape({
            avatar: PropTypes.string,
            name: PropTypes.string,
            date: PropTypes.date,
        })
    ),
};

StatusTable.defaultProps = {
    headers: [],
    statusData: [],
};

export default StatusTable;