import React from 'react';
import PropTypes from 'utils/propTypes';
import { MdDelete, MdModeEdit } from 'react-icons/md';
import { Table } from 'reactstrap';

const RulesTable = ({ headers, rulesData, ...restProps }) => {
    return (
        <Table responsive hover {...restProps}>
            <thead>
                <tr className="text-capitalize align-middle text-center">
                    {headers.map((item, index) => <th key={index}>{item}</th>)}
                </tr>
            </thead>
            <tbody>
                {rulesData.map(({ id, jobName, source, target }) => (
                    <tr key={id}>
                        <td className="align-middle text-center">{jobName}</td>
                        <td className="align-middle text-center">{source}</td>
                        <td className="align-middle text-center">{target}</td>
                        <td className="align-middle text-center">
                            <MdModeEdit size={25} color='grey' className="mr-3 can-click"/>
                            <MdDelete size={25} className="can-click text-danger"/>
                        </td>
                    </tr>
                ))}
            </tbody>
        </Table>
    );
};

RulesTable.propTypes = {
    headers: PropTypes.node,
    rulesData: PropTypes.arrayOf(
        PropTypes.shape({
            avatar: PropTypes.string,
            name: PropTypes.string,
            date: PropTypes.date,
        })
    ),
};

RulesTable.defaultProps = {
    headers: [],
    rulesData: [],
};

export default RulesTable;
