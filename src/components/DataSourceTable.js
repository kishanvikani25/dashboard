import React, { useState } from 'react';
import PropTypes from 'utils/propTypes';
import { MdDelete, MdModeEdit } from 'react-icons/md';
import { Table, Modal, ModalBody, ModalHeader, ModalFooter, Button } from 'reactstrap';
import PageSpinner from './PageSpinner';

const DataSourceTable = ({ headers, dataSources, dataSourcesLoading, deleteDataSource, toggleModal }) => {
  const [openDeleteModal, toggleDeleteModal] = useState(false);
  const [selectedDataSourceId, setDataSourceId] = useState(0);
  const deleteConfirmation = (id) => {
    setDataSourceId(id);
    toggleDeleteModal(true);
  }
  return (
    <React.Fragment>
      <Table responsive hover>
        <thead>
          <tr className="text-capitalize align-middle text-center">
            {headers.map((item, index) => <th key={index}>{item}</th>)}
          </tr>
        </thead>
        {dataSourcesLoading ? <PageSpinner large={true} /> :
          <tbody>
            {dataSources.map(({ id, dataSourceName, dataSourceType, databaseName, databaseType, host, port, userName, password }, index) => (
              <tr key={id}>
                <td className="align-middle text-center">{dataSourceName}</td>
                <td className="align-middle text-center">{dataSourceType}</td>
                <td className="align-middle text-center">{databaseName}</td>
                <td className="align-middle text-center">{databaseType}</td>
                <td className="align-middle text-center">{host}</td>
                <td className="align-middle text-center">{port}</td>
                <td className="align-middle text-center">{userName}</td>
                <td className="align-middle text-center">{password}</td>
                <td className="align-middle text-center">
                  <MdModeEdit size={25} color='grey' className="mr-2 can-click" onClick={() => toggleModal(id)}/>
                  <MdDelete size={25} className="can-click text-danger" onClick={() => deleteConfirmation(id)} />
                </td>
              </tr>
            ))}
          </tbody>
        }
      </Table>
      <Modal isOpen={openDeleteModal} toggle={() => toggleDeleteModal(false)} >
        <ModalHeader toggle={() => toggleDeleteModal(false)}>Please Confirm</ModalHeader>
        <ModalBody>
          Are you sure you want to delete this Data Source?
          </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => deleteDataSource(selectedDataSourceId)}>Delete</Button>{' '}
          <Button color="secondary" onClick={() => toggleDeleteModal(false)}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  );
};

DataSourceTable.propTypes = {
  headers: PropTypes.node,
  dataSources: PropTypes.arrayOf(
    PropTypes.shape({
      avatar: PropTypes.string,
      name: PropTypes.string,
      date: PropTypes.date,
    })
  ),
};

DataSourceTable.defaultProps = {
  headers: [],
  dataSources: [],
};

export default DataSourceTable;
