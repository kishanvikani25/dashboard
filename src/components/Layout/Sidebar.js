import sthandila_logo from 'assets/img/logo/sthandila_v2.png';
import React from 'react';
import {
  MdViewList,
  MdInsertChart,
  MdKeyboardArrowDown,
  MdContentCopy,
  MdSchedule,
  MdLockOutline,
  MdDataUsage,
  MdUpdate,
} from 'react-icons/md';
import { NavLink } from 'react-router-dom';
import {
  // UncontrolledTooltip,
  Collapse,
  Nav,
  Navbar,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import bn from 'utils/bemnames';

const pageContents = [
  { to: '/replication-rules', name: 'Rules', exact: false, Icon: MdViewList },
  { to: '/replication-scheduling', name: 'Scheduling', exact: false, Icon: MdSchedule },
];

const dataSources = { to: '/data-sources', name: 'Data Sources', exact: false, Icon: MdInsertChart }
const deIdentification = { to: '/de-identification', name: 'De - Identification', exact: false, Icon: MdLockOutline }
const profiler = { to: '/profiler', name: 'Profiler', exact: false, Icon: MdDataUsage }
const status = { to: '/migration-status', name: 'Migration Status', exact: false, Icon: MdUpdate }

const bem = bn.create('sidebar');

class Sidebar extends React.Component {
  state = {
    isOpenReplication: false,
  };

  handleClick = name => () => {
    this.setState(prevState => {
      const isOpen = prevState[`isOpen${name}`];
      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  render() {
    return (
      <aside className={bem.b()}>
        {/* <div className={bem.e('background')} style={sidebarBackground} /> */}
        <div className={bem.e('content')}>
          <Navbar>
            <BSNavLink
              id="Home"
              tag={NavLink}
              to="/data-sources"
              activeClassName="active"
              exact={true}
            >
              <span className="navbar-brand d-flex">
                <img
                  src={sthandila_logo}
                  width="40"
                  height="30"
                  className="pr-2"
                  alt=""
                />
                <span className="text-white">
                  Sthandila Inc
              </span>
              </span>
            </BSNavLink>
          </Navbar>
          <Nav vertical>
            <NavItem key={0} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${dataSources.name}-${0}`}
                tag={NavLink}
                to={dataSources.to}
                activeClassName="active"
                exact={dataSources.exact}
              >
                <dataSources.Icon className={bem.e('nav-item-icon')} />
                <span className="">{dataSources.name}</span>
              </BSNavLink>
            </NavItem>

            <NavItem
              className={bem.e('nav-item')}
              onClick={this.handleClick('Replication')}
            >
              <BSNavLink className={bem.e('nav-item-collapse')}>
                <div className="d-flex">
                  <MdContentCopy className={bem.e('nav-item-icon')} />
                  <span className="">Replication</span>
                </div>
                <MdKeyboardArrowDown
                  className={bem.e('nav-item-icon')}
                  style={{
                    padding: 0,
                    transform: this.state.isOpenReplication
                      ? 'rotate(0deg)'
                      : 'rotate(-90deg)',
                    transitionDuration: '0.3s',
                    transitionProperty: 'transform',
                  }}
                />
              </BSNavLink>
            </NavItem>
            <Collapse isOpen={this.state.isOpenReplication}>
              {pageContents.map(({ to, name, exact, Icon }, index) => (
                <NavItem key={index} className={bem.e('nav-item')}>
                  <BSNavLink
                    id={`navItem-${name}-${index}`}
                    tag={NavLink}
                    to={to}
                    activeClassName="active"
                    exact={exact}
                  >
                    <Icon className={bem.e('nav-item-icon')} />
                    <span className="">{name}</span>
                  </BSNavLink>
                </NavItem>
              ))}
            </Collapse>

            <NavItem key={1} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${deIdentification.name}-${1}`}
                tag={NavLink}
                to={deIdentification.to}
                activeClassName="active"
                exact={deIdentification.exact}
              >
                <deIdentification.Icon className={bem.e('nav-item-icon')} />
                <span className="">{deIdentification.name}</span>
              </BSNavLink>
            </NavItem>

            <NavItem key={2} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${profiler.name}-${2}`}
                tag={NavLink}
                to={profiler.to}
                activeClassName="active"
                exact={profiler.exact}
              >
                <profiler.Icon
                  className={bem.e('nav-item-icon')}
                />
                <span className="">{profiler.name}</span>
              </BSNavLink>
            </NavItem>

            <NavItem key={3} className={bem.e('nav-item')}>
              <BSNavLink
                id={`navItem-${status.name}-${3}`}
                tag={NavLink}
                to={status.to}
                activeClassName="active"
                exact={status.exact}
              >
                <status.Icon className={bem.e('nav-item-icon')} />
                <span className="">{status.name}</span>
              </BSNavLink>
            </NavItem>
          </Nav>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
