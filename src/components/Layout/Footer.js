import React from 'react';

import { Navbar, Nav, NavItem } from 'reactstrap';

import SourceLink from 'components/SourceLink';

const Footer = () => {
  return (
    <Navbar>
      <Nav navbar>
        <NavItem>
          Visit us at our official <SourceLink>Web site</SourceLink>
        </NavItem>
      </Nav>
      <span className="float-right">Copyright © Sthandila 2019</span>
    </Navbar>
  );
};

export default Footer;
