import React from 'react';
import PropTypes from 'utils/propTypes';
import { Media } from 'reactstrap';
import { MdDoneAll, MdWarning } from 'react-icons/md';

const Notifications = ({ notificationsData }) => {

  const getIcon = (icon) => {
    switch (icon) {
      case 'MdDone': return <MdDoneAll size={25} className="text-success"/>
      case 'MdClear': return <MdWarning size={25} className="text-danger"/>
      default : return
    }
  }
  return (
    notificationsData &&
    notificationsData.length &&
    notificationsData.map(({ id, icon, message, time }) => (
      <Media key={id} className="pb-2">
        <Media left className="align-self-center pr-3">
          {getIcon(icon)}
        </Media>
        <Media body middle className="align-self-center">
          {message}
        </Media>
        <Media right className="align-self-center">
          <small className="text-muted">{time}</small>
        </Media>
      </Media>
    ))
  );
};

Notifications.propTypes = {
  notificationsData: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.ID,
      icon: PropTypes.string,
      message: PropTypes.node,
      time: PropTypes.time,
    })
  ),
};

Notifications.defaultProps = {
  notificationsData: [],
};

export default Notifications;
