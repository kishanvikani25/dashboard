import React from 'react';
import {
  Button, Card, CardBody, Input, FormGroup, ModalHeader, Form,
  CardHeader, Col, Row, Label, Modal, ModalBody, ModalFooter
} from 'reactstrap';
import Page from 'components/Page';
import SearchInput from 'components/SearchInput';
import RulesTable from 'components/RulesTable';
import { rulesData } from 'demos/dashboardPage';

class RulesPage extends React.Component {
  state = {
    isOpen: false
  }

  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    return (
      <Page title="Rules">
        <Row>
          <Col md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <Row>
                  <Col md="6" sm="6" xs="6" className="mb-0">
                    <SearchInput />
                  </Col>
                  <Col md="6" sm="6" xs="6" className="mb-0">
                    <Button className="float-right btn-sm" color='primary'
                    onClick={this.toggleModal}
                    > Add Rule</Button>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <RulesTable
                  headers={[
                    'Job Name',
                    'Source',
                    'Target',
                    'Action'
                  ]}
                  rulesData={rulesData}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal className="modal-lg" isOpen={this.state.isOpen} toggle={this.toggleModal} backdrop={true}>
          <ModalHeader toggle={this.toggleModal}>Add Rule</ModalHeader>
          <ModalBody>
            <Form>
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="exampleEmail">Job Name</Label>
                    <Input type="text" name="email" id="exampleEmail" placeholder="Enter Job Name" />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="exampleEmail">Source</Label>
                    <Input type="select" name="select" id="exampleSelect" >
                      <option>Database 1</option>
                      <option>Database 2</option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="examplePassword">Target</Label>
                    <Input type="select" name="select" id="exampleSelect" >
                      <option>Database 3</option>
                      <option>Database 4</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="exampleEmail">Source Schema</Label>
                    <Input type="select" name="select" id="exampleSelect" >
                      <option>Schema 1</option>
                      <option>Schema 2</option>
                    </Input>
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                  <Label for="examplePassword">Target Schema</Label>
                    <Input type="select" name="select" id="exampleSelect" >
                      <option>Schema 3</option>
                      <option>Schema 4</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  TBD
                </Col>
                <Col md={6}>
                  TBD
                </Col>
              </Row>

            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggleModal}>Submit</Button>{' '}
            <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </Page>
    );
  }
};

export default RulesPage;
