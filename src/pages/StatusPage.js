import Page from 'components/Page';
import React from 'react';
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import StatusTable from 'components/StatusTable';

const statusData = [
  {
    id: 1,
    migration: 'Migration 1',
    color: 'success',
    percentage: 64,
    status: 'In Progress'
  },
  {
    id: 2,
    migration: 'Migration 2',
    color: 'success',
    percentage: 100,
    status: 'Done',
  },
  {
    id: 3,
    migration: 'Migration 3',
    color: 'danger',
    percentage: 77,
    status: 'Failed'
  },
  {
    id: 4,
    migration: 'Migration 4',
    color: 'success',
    percentage: 39,
    status: 'In Progress'
  },
];

const StatusPage = () => {
  return (
    <Page title="Migration Status">
      <Row>
        <Col md="12" sm="12" xs="12">
          <Card>
            <CardHeader>Check Your Migration Status here</CardHeader>
            <CardBody>
            <StatusTable
                  headers={[
                    'Migration',
                    'Status',
                  ]}
                  statusData={statusData}
                />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Page>
  );
};

export default StatusPage;
