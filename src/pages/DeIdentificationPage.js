import React from 'react';
import {
  Row, Col,
  Label, Card,
  CardHeader, Input,
  CardBody,
} from 'reactstrap';
import SchedulingTable from 'components/SchedulingTable';
import Page from 'components/Page';
import { schedulingData } from 'demos/dashboardPage';

class DeIdentificationPage extends React.Component {
  state = {

  };

  render() {
    return (
      <Page title="De-IdentificationPage">
        <Row>
          <Col md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <Row>
                  <Col md="3" sm="4" xs="4" className="mb-0">
                    <Label for="examplePassword">Select Job Name</Label>
                    <Input size="sm" type="select" name="select" id="exampleSelect" placeholder="Select Data Source type">
                      <option>Job 1</option>
                      <option>Job 2</option>
                    </Input>
                  </Col>
                  <Col md="3" sm="4" xs="4" className="mb-0">
                    <Label for="examplePassword">Select Column Name</Label>
                    <Input size="sm" type="select" name="select" id="exampleSelect" placeholder="Select Data Source type">
                      <option>Column 1</option>
                      <option>Column 2</option>
                    </Input>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <SchedulingTable
                  headers={[
                    'Job Name',
                    'Table Name',
                    'Column Name',
                    'De-identification Type'
                  ]}
                  schedulingData={schedulingData}
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Page>
    );
  }
}

export default DeIdentificationPage;
