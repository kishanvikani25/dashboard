import Page from 'components/Page';
import DataSourceTable from 'components/DataSourceTable';
import SearchInput from 'components/SearchInput';
import { dataSources, dataSourcesDefaultValue } from 'demos/dashboardPage';
import React from 'react';
import {
  Button, Modal, ModalHeader,
  Card, ModalBody, ModalFooter,
  CardBody, CardHeader, Col,
  Row, Form, FormGroup, Label, Input
} from 'reactstrap';
import { dataSourceActions } from '../actions';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PageSpinner from '../components/PageSpinner';


class DataSourcePage extends React.Component {
  constructor() {
    super();
    this.state = {
      isFormInvalid: false,
      isOpen: false,
      formData: dataSourcesDefaultValue,
    }
  }

  toggleModal = (id) => {
    if (!this.state.isOpen) {
      if (id) {
        const editDataSource = dataSources.find(dataSource => dataSource.id === id);
        this.setState({ formData: editDataSource });
      } else {
        this.setState({ formData: dataSourcesDefaultValue });
      }
    }
    this.setState({ isFormInvalid: false, isOpen: !this.state.isOpen });
  }

  handleChange = (event) => {
    const { id, value } = event.target;
    this.setState({ formData: { ...this.state.formData, [id]: value }, })
  }

  submitForm = () => {
    const { formData } = this.state;
    const invalidFormValues = Object.keys(formData).filter(key => formData[key] === "").length > 0;
    this.setState({ isFormInvalid: invalidFormValues }, () => {
      if (!this.state.isFormInvalid) {
        this.setState({ isOpen: !this.state.isOpen });
        if (this.state.formData.id) {
          this.props.dataSourceActions.postDataSources();
          // alert('Edit');
        } else {
          this.props.dataSourceActions.postDataSources();
        }
      }
    });
  }


  handleSearch = () => {

  }
  render() {
    const { formData } = this.state;
    return (
      <Page title="Data Sources">
        <Row>
          <Col md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <Row>
                  <Col md="6" sm="6" xs="6" className="mb-0">
                    <SearchInput onChange={this.handleSearch} />
                  </Col>
                  <Col md="6" sm="6" xs="6" className="text-right mb-0">
                    <Button className="btn-sm" color='primary' onClick={() => this.toggleModal(0)}>
                      Add Data Source
                    </Button>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                {this.props.dataSourcesLoading
                  ? <PageSpinner large={true} />
                  : <DataSourceTable
                    headers={[
                      'Name',
                      'Type',
                      'DB Name',
                      'DB Type',
                      'Host',
                      'Port',
                      'Username',
                      'Password',
                      'Action'
                    ]}
                    dataSources={dataSources}
                    toggleModal={this.toggleModal}
                  />
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Modal className="modal-lg" isOpen={this.state.isOpen} toggle={this.toggleModal} backdrop={true} keyboard={false}>
          <ModalHeader toggle={this.toggleModal}>{this.state.formData.id ? `Edit Data Source` : `Add Data Source`}</ModalHeader>
          <ModalBody>
            <Form>
              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="dataSourceName">Data Source Name</Label>
                    <Input type="text" name="dataSourceName" id="dataSourceName" value={formData.dataSourceName}
                      onChange={this.handleChange} placeholder="Enter Name for Data Source" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="dataSourceType">Select Data Source Type</Label>
                    <Input type="select" name="dataSourceType" id="dataSourceType" value={formData.dataSourceType}
                      onChange={this.handleChange} placeholder="Select Data Source type">
                      <option>Source</option>
                      <option>Target</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="userName">Username</Label>
                    <Input type="text" name="userName" id="userName" value={formData.userName}
                      onChange={this.handleChange} placeholder="Enter Username" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" name="password" id="password" value={formData.password}
                      onChange={this.handleChange} placeholder="Enter Password" />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="host">Host</Label>
                    <Input type="text" name="host" id="host" value={formData.host}
                      onChange={this.handleChange} placeholder="Enter Host I.P." />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="port">Port</Label>
                    <Input type="password" name="port" id="port" value={formData.port}
                      onChange={this.handleChange} placeholder="Enter Port Number" />
                  </FormGroup>
                </Col>
              </Row>

              <Row form>
                <Col md={6}>
                  <FormGroup>
                    <Label for="databaseName">Database Name</Label>
                    <Input type="text" name="databaseName" id="databaseName" value={formData.databaseName}
                      onChange={this.handleChange} placeholder="Enter Database Name" />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="databaseType">Database Type</Label>
                    <Input type="select" name="databaseType" id="databaseType" value={formData.databaseType}
                      onChange={this.handleChange} placeholder="Select Data Source type">
                      <option>MySQL</option>
                      <option>MS-SQL</option>
                      <option>Oracle</option>
                      <option>MongoDB</option>
                      <option>MariaDB</option>
                      <option>PostgreSQL</option>
                    </Input>
                  </FormGroup>
                </Col>
              </Row>

              {this.state.isFormInvalid &&
                (<Row form>
                  <Col>
                    <p className="text-danger">Please fill out all the values</p>
                  </Col>
                </Row>)}

            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.submitForm}>Submit</Button>{' '}
            <Button color="secondary" onClick={this.toggleModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    dataSources: state.dataSourceReducer.dataSources || [],
    dataSourcesLoading: state.dataSourceReducer.dataSourcesLoading
  }
}

function mapDispatchToProps(dispatch) {
  return { dataSourceActions: bindActionCreators(dataSourceActions, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(DataSourcePage);