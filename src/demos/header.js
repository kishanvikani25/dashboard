
export const notificationsData = [
  {
    id: 1,
    icon: 'MdDone',
    message: 'Data Migration from SQL to MongoDB was completed successfully.',
    time: '3 min ago',
  },
  {
    id: 2,
    icon: 'MdClear',
    message: 'Data Migration from MariaDB to MySQL failed.',
    time: '1 hour ago',
  },
];
